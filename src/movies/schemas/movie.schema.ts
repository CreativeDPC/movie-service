import { ApiProperty } from '@nestjs/swagger';
import { GenreEnum } from './../enums/genre.enum';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as moment from 'moment';
import { Document } from 'mongoose';
import { Exclude } from 'class-transformer';

export type MovieDocument = Movie & Document;

@Schema()
export class Movie {

  @ApiProperty()
  @Prop({
      required:true,
      maxlength: 250
  })
  title: string;

  @ApiProperty()
  @Prop({
    required:true,
    maxlength: 20
  })
  year: string;

  @ApiProperty()
  @Prop({
    required:true,
    maxlength: 20
  })
  runtime: string;  

  @ApiProperty()
  @Prop({
    required:true,
    enum: GenreEnum
  })
  genre: string; 

  @ApiProperty()
  @Prop({
    required:true
  })
  poster: string; 

  @Prop({
    required:true,
    default: true
  })
  active: boolean; 

  @Prop({
    default: moment().format()
  })
  dateCreate: string; 
  
}

export const MovieSchema = SchemaFactory.createForClass(Movie);