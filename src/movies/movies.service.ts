import { Movie, MovieDocument } from './schemas/movie.schema';
import { Injectable } from '@nestjs/common';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class MoviesService {
  private hidenProperties = {dateCreate:0, active:0, __v:0};

  constructor(
    @InjectModel(Movie.name) private readonly movieModel: Model<MovieDocument>
  ){}

  async create(createMovieDto: CreateMovieDto): Promise<Movie> {
    const createdMovie = new this.movieModel(createMovieDto);
    return createdMovie.save();
  }

  async findAll(): Promise<Movie[]> {
    return this.movieModel.find({active:true},this.hidenProperties).exec();
  }

  async findOne(id: string):Promise<Movie> {
    return this.movieModel.findOne({_id:id, active:true}, this.hidenProperties);
  }

  async update(id: string, updateMovieDto: UpdateMovieDto): Promise<any> {
    return await this.movieModel.updateOne(
      { _id: {$eq: id}}, 
      {$set: updateMovieDto});
  }

  async remove(id: string):Promise<any> {
    return await this.movieModel.deleteOne({_id: id});
  }

}
