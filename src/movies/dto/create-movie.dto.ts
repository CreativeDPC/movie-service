import { GenreEnum } from './../enums/genre.enum';
import { ApiProperty } from "@nestjs/swagger";

export class CreateMovieDto {
    @ApiProperty()
    readonly title: string;
    @ApiProperty()
    readonly year: string;
    @ApiProperty()
    readonly runtime: string;
    @ApiProperty()
    readonly poster: string;
    @ApiProperty({enum: GenreEnum})
    readonly genre:string;    
}
