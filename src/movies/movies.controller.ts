import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Post, Body, Put, Param, Delete, HttpException, HttpStatus, NotFoundException } from '@nestjs/common';
import { MoviesService } from './movies.service';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';


@ApiTags('Movies')
@Controller('movies')
export class MoviesController {
  constructor(private readonly moviesService: MoviesService) {}

  @Post()
  async create(@Body() createMovieDto: CreateMovieDto) {
    try {
      await this.moviesService.create(createMovieDto);  
    } catch (error) {
      this.exception(error);
    }    
  }

  @Get()
  async findAll() {
    try {
      return this.moviesService.findAll(); 
    } catch (error) {
      this.exception(error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    try {
      const movie = await this.moviesService.findOne(id); 
      if(movie) return movie
      else throw new HttpException('No existe el registro solicitado.', HttpStatus.NO_CONTENT)
      
    } catch (error) {
      this.exception(error);
    }    
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updateMovieDto: UpdateMovieDto) {
    try {
      return this.moviesService.update(id, updateMovieDto);
    } catch (error) {
      this.exception(error);
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      return this.moviesService.remove(id);
    } catch (error) {
      this.exception(error);
    }
  }

  private exception(ex) {
    throw new HttpException(ex, HttpStatus.BAD_REQUEST);
  }
}
