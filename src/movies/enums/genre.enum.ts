export enum GenreEnum {
    ACTION = 'Action',
    ADVENTURE = 'Adventure',
    TERROR = 'Terror',
}