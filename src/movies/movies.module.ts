import { MovieSchema, Movie } from './schemas/movie.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule, Module } from '@nestjs/common';
import { MoviesService } from './movies.service';
import { MoviesController } from './movies.controller';

@Module({
  imports:[
    HttpModule,
    MongooseModule.forFeature([
      { name: Movie.name, schema: MovieSchema }
    ])
  ],
  controllers: [MoviesController],
  providers: [MoviesService]
})
export class MoviesModule {}
