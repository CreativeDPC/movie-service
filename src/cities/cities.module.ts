import { CitySchema } from './schemas/city.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Module, HttpModule } from '@nestjs/common';
import { CitiesService } from './cities.service';
import { CitiesController } from './cities.controller';
import { Movie, MovieSchema } from 'src/movies/schemas/movie.schema';
import { City } from 'src/cities/schemas/city.schema';

@Module({
  imports:[
    HttpModule,
    MongooseModule.forFeature([
      { name: Movie.name, schema: MovieSchema },
      { name: City.name, schema: CitySchema },
    ])
  ],  
  controllers: [CitiesController],
  providers: [CitiesService]
})
export class CitiesModule {}
