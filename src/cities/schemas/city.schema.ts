import { Movie } from '../../movies/schemas/movie.schema';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import * as moment from 'moment';

export type CityDocument = City & Document;

@Schema()
export class City {

  @Prop({
      required:true,
      maxlength: 250
  })
  name: string;

  @Prop({
    required:true,
    maxlength: 250
  })
  abbreviation: string;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Movie' }] })
  movies: Movie[];

  @Prop({
    required:true,
    default: true
  })
  active: boolean; 

  @Prop({
    required:true,
    default: moment().format()
  })
  dateCreate: string;   
  
}

export const CitySchema = SchemaFactory.createForClass(City);