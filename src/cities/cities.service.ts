import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { CreateCityDto } from './dto/create-city.dto';
import { UpdateCityDto } from './dto/update-city.dto';
import { City, CityDocument } from './schemas/city.schema';

@Injectable()
export class CitiesService {
  private fieldHiden =  {dateCreate: 0, active:0, __v:0 };

  constructor(
    @InjectModel(City.name) private readonly cityModel: Model<CityDocument>
  ){}

  async create(createMovieDto: CreateCityDto): Promise<City> {
    const createdCity = new this.cityModel(createMovieDto);
    return createdCity.save();
  }

  async findAll(): Promise<City[]> {
    return this.cityModel
                .find({}, {...this.fieldHiden, movies:0})
                .populate({ path: 'movies', model: 'Movie' })
                .exec();
  }

  async findOne(id: string):Promise<City> {
    
    return this.cityModel
            .findOne({_id:id}, this.fieldHiden)
            .populate({ path: 'movies', model: 'Movie', select: this.fieldHiden }, )
            .exec()
            
  }

  async update(id: string, updateMovieDto: UpdateCityDto): Promise<any> {
    return await this.cityModel.updateOne(
      { _id: {$eq: id}}, 
      {$set: updateMovieDto});
  }

  async remove(id: string):Promise<any> {
    return await this.cityModel.deleteOne({_id: id});
  }
}
