import { CreateMovieDto } from './../../movies/dto/create-movie.dto';
import { ApiProperty } from '@nestjs/swagger';
import { Movie } from "src/movies/schemas/movie.schema";

export class CreateCityDto {
    @ApiProperty()
    readonly name:string;
    @ApiProperty()
    readonly abbreviation:string;
    @ApiProperty({ type: [Movie] })
    readonly movies:Movie[];
}