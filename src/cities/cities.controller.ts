import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Post, Body, Put, Param, Delete, HttpException, HttpStatus } from '@nestjs/common';
import { CitiesService } from './cities.service';
import { CreateCityDto } from './dto/create-city.dto';
import { UpdateCityDto } from './dto/update-city.dto';

@ApiTags('Cities')
@Controller('cities')
export class CitiesController {
  constructor(private readonly citiesService: CitiesService) {}

  @Post()
  create(@Body() createCityDto: CreateCityDto) {
    try {
      return this.citiesService.create(createCityDto);
    } catch (error) {
      this.exception(error);
    }
  }

  @Get()
  findAll() {
    try {
      return this.citiesService.findAll();
    } catch (error) {
      this.exception(error);
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    try {
      return this.citiesService.findOne(id);
    } catch (error) {
      this.exception(error);
    }
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateCityDto: UpdateCityDto) {
    try {
      return this.citiesService.update(id, updateCityDto);
    } catch (error) {
      this.exception(error);
    }
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    try {
      return this.citiesService.remove(id);
    } catch (error) {
      this.exception(error);
    }
  }

  private exception(ex) {
    throw new HttpException(ex, HttpStatus.BAD_REQUEST);
  }
}
