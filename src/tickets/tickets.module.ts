import { Ticket, TicketSchema } from './schemas/ticket.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Module, HttpModule } from '@nestjs/common';
import { TicketsService } from './tickets.service';
import { TicketsController } from './tickets.controller';
import { Movie, MovieSchema } from 'src/movies/schemas/movie.schema';

@Module({
  imports:[
    HttpModule,
    MongooseModule.forFeature([
      { name: Movie.name, schema: MovieSchema },
      { name: Ticket.name, schema: TicketSchema },
    ])
  ],    
  controllers: [TicketsController],
  providers: [TicketsService]
})
export class TicketsModule {}
