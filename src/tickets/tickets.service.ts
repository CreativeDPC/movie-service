import { InjectModel } from '@nestjs/mongoose';
import { Injectable } from '@nestjs/common';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { Ticket, TicketDocument } from './schemas/ticket.schema';
import { Model } from 'mongoose';

@Injectable()
export class TicketsService {
  private fieldHiden =  {dateCreated: 0, active:0, __v:0 };

  constructor(
    @InjectModel(Ticket.name) private readonly ticketModel: Model<TicketDocument>
  ){}

  async create(createTicketDto: CreateTicketDto): Promise<Ticket> {
    const createdTicket = new this.ticketModel(createTicketDto);
    return createdTicket.save();
  }

  async findAll(): Promise<Ticket[]> {
    return this.ticketModel
              .find({}, this.fieldHiden)
              .populate({ path: 'movie', model: 'Movie', select: {...this.fieldHiden, dateCreate:0} })
              .exec();
  }

  async findOne(id: string):Promise<Ticket> {
    return this.ticketModel
            .findOne({_id:id}, this.fieldHiden)
            .populate({ path: 'movie', model: 'Movie', select: {...this.fieldHiden, dateCreate:0} })
            .exec()
  }

  async update(id: string, updateTicketDto: UpdateTicketDto): Promise<any> {
    return await this.ticketModel.updateOne(
      { _id: {$eq: id}}, 
      {$set: updateTicketDto});
  }

  async remove(id: string):Promise<any> {
    return await this.ticketModel.deleteOne({_id: id});
  }
}
