import { ApiProperty } from '@nestjs/swagger';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Movie } from 'src/movies/schemas/movie.schema';

export type TicketDocument = Ticket & Document;

@Schema()
export class Ticket {

  @ApiProperty()
  @Prop({
      required:true
  })
  scheduledDate: string;

  @ApiProperty()
  @Prop({
    required:true,
    maxlength: 100
  })
  username: string;

  @ApiProperty()
  @Prop({ required:true ,type: mongoose.Schema.Types.ObjectId, ref: 'Movie' })
  movie: Movie;  

  @Prop({
    required:true,
    default: moment().format()
  })
  dateCreated: string; 
  
}

export const TicketSchema = SchemaFactory.createForClass(Ticket);