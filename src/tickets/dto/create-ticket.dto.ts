import { ApiProperty } from '@nestjs/swagger';
import { Movie } from 'src/movies/schemas/movie.schema';

export class CreateTicketDto {
    @ApiProperty()
    readonly scheduledDate:string;
    @ApiProperty()
    readonly username:string;
    @ApiProperty({ type: Movie })
    readonly movie:Movie;
}
