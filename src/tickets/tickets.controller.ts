import { JwtAuthGuard } from './../auth/guards/jwt-auth.guard';
import { Controller, Get, Post, Body, Put, Param, Delete, HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { TicketsService } from './tickets.service';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('Tickets')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@Controller('tickets')
export class TicketsController {
  constructor(private readonly ticketsService: TicketsService) {}

  @Post()
  create(@Body() createCityDto: CreateTicketDto) {
    try {
      return this.ticketsService.create(createCityDto);
    } catch (error) {
      this.exception(error);
    }
  }

  @Get()
  findAll() {
    try {
      return this.ticketsService.findAll();
    } catch (error) {
      this.exception(error);
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    try {
      return this.ticketsService.findOne(id);
    } catch (error) {
      this.exception(error);
    }
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateTicketDto: UpdateTicketDto) {
    try {
      return this.ticketsService.update(id, updateTicketDto);
    } catch (error) {
      this.exception(error);
    }
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    try {
      return this.ticketsService.remove(id);
    } catch (error) {
      this.exception(error);
    }
  }

  private exception(ex) {
    throw new HttpException(ex, HttpStatus.BAD_REQUEST);
  }
}
