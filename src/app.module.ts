import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MoviesModule } from './movies/movies.module';
import { CitiesModule } from './cities/cities.module';
import { TicketsModule } from './tickets/tickets.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://test:ZZYYxx22--@cluster0.9lgbj.mongodb.net/movies?retryWrites=true&w=majority'),
    MoviesModule,
    CitiesModule,
    TicketsModule,
    AuthModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
